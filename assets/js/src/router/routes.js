import App from "@/App"
import AppHeader from "@/layout/AppHeader"
import AppFooter from "@/layout/AppFooter"

function load (component) {
  // this generates a separate chunk (about.[hash].js) for this component/route
  // which is lazy-loaded when the route is visited.
  return () => import(/* webpackChunkName: "../js/[request]" */ `@/views/${component}`)
}
export default [
  {
    path: '/',
    components: {
      header: AppHeader,
      default: load('Home'),
      footer: AppFooter
    },
    children: [
      {
        path: '',
        name: 'home',
        component: load('Home')
      },
      {
        path: 'about',
        name: 'about',
        component: load('About')
      }
    ]
  }  
]
