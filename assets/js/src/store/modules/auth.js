const state = {
  user: null,
  token: ''
}

const getters ={
  isAuthentificated: (state) => {
    state.token = state.token || localStorage.getItem('authToken')
    return !!state.token
  },
  authHeaders: (state) => {
    return {
      'Authorization': 'Bearer ' + state.token
    }
  }
}

const mutations = {
  setAuthToken(state, token) {
    state.token = token
  },
  setCurrentUser(state, user) {
    state.user = user
  },
  setUserMeta(state, data) {
    state.token = data.meta.token
    state.user = data.user
    localStorage.setItem('authToken', data.meta.token)
  },
  clearUserMeta(state) {
    state.token = ''
    state.user = null
    localStorage.removeItem('authToken')
  }
}
const actions = {
  signin({ commit }, user) {
    return axios.post('api/sign-in', user)
      .then(response => {
        commit('setUserMeta', response.data)
      })
  },

  signup({commit}, user) {
    return axios.post('api/sign-up', user)
      .then(response => {
        commit('setUserMeta', response.data)
      })
  },

  getCurrentUser({ commit }, headers) {
    return axios.get('api/session', { headers: headers })
      .then(response => {
        commit('setCurrentUser', response.data.data)
      },
      error => {
        commit('setCurrentUser', null)
      })
  },

  logout({ commit }, headers) {
    return axios.delete('api/session', { headers: headers })
      .then(response => {
        commit('clearUserMeta')
      })
  },

}

export default {
  state,
  getters,
  mutations,
  actions
}
