const state = {
  list: [],
  nearby: [],
  preffered: [],
}

const getters = {
  
}

const mutations = {
  setShopsList(state, shops) {
    state.list = shops
    state.nearby = shops.filter(shop => {
      let disliked_at = shop.action && shop.action.disliked_at ? new Date(shop.action.disliked_at) : null
      return (!disliked_at || (Math.abs(Date.now() - disliked_at) / 36e5 > 2)) && !(shop.action && shop.action.liked_at)
    })
    state.preffered = shops.filter(shop => shop.action && shop.action.liked_at)
  },
  updateShopsList(state, shop) {
    let index = state.list.map(shop => shop.id).indexOf(shop.id)
    if (~index) {
      state.list[index] = shop
      
      state.nearby = state.list.filter(shop => {
        let disliked_at = shop.action && shop.action.disliked_at ? new Date(shop.action.disliked_at) : null
        return (!disliked_at || (Math.abs(Date.now() - disliked_at) / 36e5 > 2)) && !(shop.action && shop.action.liked_at)
      })
      state.preffered = state.list.filter(shop => shop.action && shop.action.liked_at)
    }
  }
}
const actions = {
  getAllShops({ commit }) {
    return axios.get('api/shops/all')
      .then(response => {
        commit('setShopsList', response.data.data)
      })
  },

  getShops({ commit }) {
    return axios.get('api/shops')
      .then(response => {
        commit('setShopsList', response.data.data)
      })
  },

  likeShop({ commit }, payload) {
    return axios.post('api/shops/like', payload)
        .then( response => {
          commit('updateShopsList', response.data.data)
        })
  },

  dislikeShop({ commit }, payload) {
    return axios.post('api/shops/dislike', payload)
      .then(response => {
        commit('updateShopsList', response.data.data)
      })
  }

}

export default {
  state,
  getters,
  mutations,
  actions
}
