import Vue from 'vue'
import App from './App.vue'
import Vendor from './plugins'
import router from './router'
import store from './store'

Vue.config.productionTip = false

Vue.use(Vendor);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
