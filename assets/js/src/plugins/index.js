import Vue from 'vue'
import Vuetify from 'vuetify'
import VueIziToast from 'vue-izitoast';
import axios from 'axios'

import 'izitoast/dist/css/iziToast.min.css';
import 'vuetify/dist/vuetify.min.css'

export default {
  install(Vue) {
    window.axios = axios.create({
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('authToken')
      }
    })

    Vue.use(Vuetify)
    Vue.use(VueIziToast)
  }
}