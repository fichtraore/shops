defmodule Shops.ShopsContextTest do
  use Shops.DataCase

  alias Shops.ShopsContext

  describe "shops" do
    alias Shops.ShopsContext.Shop

    @valid_attrs %{distance: 120.5, name: "some name"}
    @update_attrs %{distance: 456.7, name: "some updated name"}
    @invalid_attrs %{distance: nil, name: nil}

    def shop_fixture(attrs \\ %{}) do
      {:ok, shop} =
        attrs
        |> Enum.into(@valid_attrs)
        |> ShopsContext.create_shop()

      shop
    end

    test "list_shops/0 returns all shops" do
      shop = shop_fixture()
      assert ShopsContext.list_shops() == [shop]
    end

    test "get_shop!/1 returns the shop with given id" do
      shop = shop_fixture()
      assert ShopsContext.get_shop!(shop.id) == shop
    end

    test "create_shop/1 with valid data creates a shop" do
      assert {:ok, %Shop{} = shop} = ShopsContext.create_shop(@valid_attrs)
      assert shop.distance == 120.5
      assert shop.name == "some name"
    end

    test "create_shop/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = ShopsContext.create_shop(@invalid_attrs)
    end

    test "update_shop/2 with valid data updates the shop" do
      shop = shop_fixture()
      assert {:ok, %Shop{} = shop} = ShopsContext.update_shop(shop, @update_attrs)
      assert shop.distance == 456.7
      assert shop.name == "some updated name"
    end

    test "update_shop/2 with invalid data returns error changeset" do
      shop = shop_fixture()
      assert {:error, %Ecto.Changeset{}} = ShopsContext.update_shop(shop, @invalid_attrs)
      assert shop == ShopsContext.get_shop!(shop.id)
    end

    test "delete_shop/1 deletes the shop" do
      shop = shop_fixture()
      assert {:ok, %Shop{}} = ShopsContext.delete_shop(shop)
      assert_raise Ecto.NoResultsError, fn -> ShopsContext.get_shop!(shop.id) end
    end

    test "change_shop/1 returns a shop changeset" do
      shop = shop_fixture()
      assert %Ecto.Changeset{} = ShopsContext.change_shop(shop)
    end
  end
end
