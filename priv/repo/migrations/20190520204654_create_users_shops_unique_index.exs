defmodule Shops.Repo.Migrations.CreateUsersShopsUniqueIndex do
  use Ecto.Migration

  def change do
      create unique_index(:users_shops, [:user_id, :shop_id], name: :users_shops_unique_index)
  end
end
