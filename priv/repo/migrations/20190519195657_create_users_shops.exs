defmodule Shops.Repo.Migrations.CreateUsersShops do
  use Ecto.Migration

  def change do
    create table(:users_shops) do
      add :user_id, references(:users)
      add :shop_id, references(:shops)
      add :liked_at, :naive_datetime
      add :disliked_at, :naive_datetime

      timestamps()
    end
  end
end
