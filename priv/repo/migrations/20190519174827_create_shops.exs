defmodule Shops.Repo.Migrations.CreateShops do
  use Ecto.Migration

  def change do
    create table(:shops) do
      add :name, :string
      add :distance, :integer

      timestamps()
    end

  end
end
