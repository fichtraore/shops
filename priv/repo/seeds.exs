# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Shops.Repo.insert!(%Shops.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Shops.Repo
alias Shops.ShopsContext.Shop

for i <- 1..20 do
  Repo.insert! %Shop{name: "Shop name #{i}", distance: :rand.uniform(1000)}
end
