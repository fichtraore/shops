defmodule Shops.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Shops.Repo

  alias Shops.Accounts.User
  alias Shops.Accounts.UserShop
  alias Shops.ShopsContext.Shop

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    result =
    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()

    case result do
      {:ok, user} -> {:ok, %User{user | password: nil}}
      _ -> result
    end
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for authentificated user.

  """
  def authenticate(email, password) do
    user = Repo.get_by(User, email: String.downcase(email))

    case check_password(user, password) do
      true -> {:ok, user}
      _ -> {:error}
    end
  end

  defp check_password(user, password) do
    case user do
      nil -> Bcrypt.no_user_verify()
      _ -> Bcrypt.verify_pass(password, user.encrypted_password)
    end
  end

  @doc """
  Returns the list of shops with user related informations.

  ## Examples

      iex> list_shops(user)
      [%UserShop{}, ...]

  """
  def list_shops(user) do
    Shop
    |> order_by(asc: :distance)
    |> Repo.all()
    |> Repo.preload(:users_shops, user: user)
  end

  @doc """
  Create user and shop relation for shops like or dislike

  """
  def create_user_shop(user, attrs) do
    result =
      case Repo.get_by(UserShop, [user_id: user.id, shop_id: attrs.shop_id] ) do
        nil  -> %UserShop{user_id: user.id, shop_id: attrs.shop_id}
        user_shop -> user_shop
      end
      |> UserShop.changeset(attrs)
      |> Repo.insert_or_update

    case result do
      {:ok, user_shop} ->
        Repo.preload(user_shop, :shop).shop
        |> Repo.preload(:users_shops, user: user)
      _ ->
        result
    end
  end

  def update_user_shop(%UserShop{} = user_shop, attrs) do
    user_shop
    |> UserShop.changeset(attrs)
    |> Repo.update()
  end
end
