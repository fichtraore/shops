defmodule Shops.Auth.Pipeline do
  use Guardian.Plug.Pipeline,
    otp_app: :shops,
    module: Shops.Auth.Guardian,
    error_handler: Shops.Auth.ErrorHandler

  plug(Guardian.Plug.VerifyHeader)
  plug(Guardian.Plug.EnsureAuthenticated)
  plug(Guardian.Plug.LoadResource)
end
