defmodule Shops.Repo do
  use Ecto.Repo,
    otp_app: :shops,
    adapter: Ecto.Adapters.Postgres
end
