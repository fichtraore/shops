defmodule Shops.ShopsContext.Shop do
  use Ecto.Schema
  import Ecto.Changeset

  alias Shops.Accounts.UserShop

  schema "shops" do
    field :distance, :integer
    field :name, :string

    has_many :users_shops, UserShop

    timestamps()
  end

  @doc false
  def changeset(shop, attrs) do
    shop
    |> cast(attrs, [:name, :distance])
    |> validate_required([:name, :distance])
    |> unique_constraint(:name)
  end
end
