defmodule Shops.Accounts.UserShop do
  use Ecto.Schema
  import Ecto.Changeset

  alias Shops.Accounts.User
  alias Shops.ShopsContext.Shop

  @derive {Jason.Encoder, only: [:liked_at, :disliked_at]}

  schema "users_shops" do
    field :liked_at, :naive_datetime
    field :disliked_at, :naive_datetime

    belongs_to :user, User
    belongs_to :shop, Shop

    timestamps()
  end

  def changeset(struct, attrs) do
    struct
    |> cast(attrs, [:user_id, :shop_id, :liked_at, :disliked_at])
    |> validate_required([:user_id, :shop_id])
    |> unique_constraint(:users_shops_unique_constraint, name: :users_shops_unique_index)
  end
end
