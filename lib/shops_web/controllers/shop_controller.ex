defmodule ShopsWeb.ShopController do
  use ShopsWeb, :controller

  alias Shops.ShopsContext
  alias Shops.ShopsContext.Shop


  action_fallback ShopsWeb.FallbackController

  def index(conn, _params) do
    shops = ShopsContext.list_shops_by_distance()
    render(conn, "index.json", shops: shops)
  end

  def create(conn, %{"shop" => shop_params}) do
    with {:ok, %Shop{} = shop} <- ShopsContext.create_shop(shop_params) do
      conn
      |> put_status(:created)
      |> render("show.json", shop: shop)
    end
  end

  def show(conn, %{"id" => id}) do
    shop = ShopsContext.get_shop!(id)
    render(conn, "show.json", shop: shop)
  end

  def update(conn, %{"id" => id, "shop" => shop_params}) do
    shop = ShopsContext.get_shop!(id)

    with {:ok, %Shop{} = shop} <- ShopsContext.update_shop(shop, shop_params) do
      render(conn, "show.json", shop: shop)
    end
  end

  def delete(conn, %{"id" => id}) do
    shop = ShopsContext.get_shop!(id)

    with {:ok, %Shop{}} <- ShopsContext.delete_shop(shop) do
      send_resp(conn, :no_content, "")
    end
  end
end
