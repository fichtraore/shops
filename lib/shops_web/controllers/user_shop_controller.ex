defmodule ShopsWeb.UserShopController do
  use ShopsWeb, :controller

  alias Shops.Accounts
  alias Shops.Auth.Guardian


  def index(conn, _params) do
    user = Guardian.Plug.current_resource(conn)
    case user do
      nil ->
        conn
        |> put_view(ShopsWeb.ShopView)
        |> render("index.json", shops: Shops.ShopsContext.list_shops_by_distance())
      _->
        conn
        |> put_view(ShopsWeb.ShopView)
        |> render("index.json", shops: Accounts.list_shops(user))
      end
  end

  def like(conn, %{"id" => id, "reset" => reset}) do
    attrs =
    case reset do
      true -> %{shop_id: id, liked_at: nil, disliked_at: nil}
      _-> %{shop_id: id, liked_at: NaiveDateTime.utc_now |> NaiveDateTime.truncate(:second), disliked_at: nil}
    end

    with shop <- Accounts.create_user_shop(Guardian.Plug.current_resource(conn), attrs) do
        conn
        |> put_view(ShopsWeb.ShopView)
        |> render("show.json", shop: shop)
    end

  end

  def dislike(conn, %{"id" => id, "reset" => reset}) do
    attrs =
    case reset do
      true -> %{shop_id: id, liked_at: nil, disliked_at: nil}
      _-> %{shop_id: id, liked_at: nil, disliked_at: NaiveDateTime.utc_now |> NaiveDateTime.truncate(:second)}
    end

    with shop <- Accounts.create_user_shop(Guardian.Plug.current_resource(conn), attrs) do
        conn
        |> put_view(ShopsWeb.ShopView)
        |> render("show.json", shop: shop)
    end
  end
end
