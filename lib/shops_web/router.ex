defmodule ShopsWeb.Router do
  use ShopsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :api_auth do
    plug Shops.Auth.Pipeline
  end

  # Other scopes may use custom stacks.
  scope "/api", ShopsWeb do
    pipe_through :api

    get "/shops/all", ShopController, :index
    post "/sign-up", UserController, :create
    post "/sign-in", SessionController, :create

  end

  scope "/api", ShopsWeb do
    pipe_through([:api, :api_auth])

    get "/session", SessionController, :show
    delete "/session", SessionController, :delete
    post "/session/refresh", SessionController, :refresh

    get "/shops", UserShopController, :index
    post "/shops/like", UserShopController, :like
    post "/shops/dislike", UserShopController, :dislike
  end

  scope "/", ShopsWeb do
    pipe_through :browser

    get "/*path", PageController, :index
  end
end
