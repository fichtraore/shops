defmodule ShopsWeb.UserShopView do
  use ShopsWeb, :view
  alias ShopsWeb.UserShopView

  def render("index.json", %{users_shops: users_shops}) do
    %{data: render_many(users_shops, UserShopView, "shop.json")}
  end

  def render("show.json", %{user_shop: user_shop}) do
    %{data: render_one(user_shop, UserShopView, "shop.json")}
  end

  def render("shop.json", %{user_shop: user_shop}) do
    %{id: user_shop.shop.id,
      name: user_shop.shop.name,
      distance: user_shop.shop.distance,
      liked_at: user_shop.liked_at,
      disliked_at: user_shop.disliked_at
    }
  end

  def render("error.json", %{error: error}) do
    %{errors: %{error: error}}
  end
end
