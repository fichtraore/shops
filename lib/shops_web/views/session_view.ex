defmodule ShopsWeb.SessionView do
  use ShopsWeb, :view
  alias ShopsWeb.UserView

  def render("show.json", %{user: user, jwt: jwt}) do
    %{
      user: render_one(user, UserView, "user.json"),
      meta: %{token: jwt}
    }
  end

  def render("delete.json", _) do
    %{ok: true}
  end

  def render("error.json", %{error: error}) do
    %{errors: %{error: error}}
  end
end
