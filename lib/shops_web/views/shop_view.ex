defmodule ShopsWeb.ShopView do
  use ShopsWeb, :view
  alias ShopsWeb.ShopView

  def render("index.json", %{shops: shops}) do
    %{data: render_many(shops, ShopView, "shop.json")}
  end

  def render("show.json", %{shop: shop}) do
    %{data: render_one(shop, ShopView, "shop.json")}
  end

  def render("shop.json", %{shop: shop}) do
    case Ecto.assoc_loaded?(shop.users_shops) do
      true ->
         %{id: shop.id,
          name: shop.name,
          distance: shop.distance,
          action: Enum.at(shop.users_shops, 0)}
      _->
        %{id: shop.id,
          name: shop.name,
          distance: shop.distance,
          action: nil}
    end
  end
end
