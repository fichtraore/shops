# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :shops,
  ecto_repos: [Shops.Repo]

# Configures the endpoint
config :shops, ShopsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "3K6jk+KuwhgN2HnO/WJi1gQaUr8zs+QDlY6jQJQs4IB9fuRZdH9kMD8xAqOj9Ugd",
  render_errors: [view: ShopsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Shops.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

# Guardian configuration
config :shops, Shops.Auth.Guardian,
        ussuer: "shops",
        secret_key: "uoRLn3/4DTAsQZsDMHCINNP9t0To0vEeyRyWpv+aX3BadzdDYCKkd7bTxWbk3O8C"
